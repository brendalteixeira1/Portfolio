$(document).ready(function()
{
    //Atributes
    var isSideBarEnable = false;

    // ------------ NAVIGATION ----------------//
    $("#menu_item1").click(function GoTo()
    {
        $('html, body').animate({scrollTop:$("#welcomePage").offset().top}, 'slow');
        if(isSideBarEnable) //in case side bar is opened, close it
            AnimateSideBar();
    });
    
    $("#menu_item2").click(function GoTo()
    {
        $('html, body').animate({scrollTop:$("#profilePage").offset().top}, 'slow');
        if(isSideBarEnable)//in case side bar is opened, close it
            AnimateSideBar();
    });
    
    $("#menu_item3").click(function GoTo()
    {
        $('html, body').animate({scrollTop:$("#portfolioPage").offset().top}, 'slow');
        if(isSideBarEnable)//in case side bar is opened, close it
            AnimateSideBar();
    });

    $("#menu_item4").click(function GoTo()
    {
        $('html, body').animate({scrollTop:$("#contactPage").offset().top}, 'slow');
        if(isSideBarEnable)//in case side bar is opened, close it
            AnimateSideBar();
    });


    // -------- Click on Icon Picture -----------//
    $("#imgLogo").click(AnimateSideBar);
    
    function AnimateSideBar()
    {
        //only animate Navbar when it is set as Sidebar
        if($(window).width () < 800) 
        {
            if(!isSideBarEnable)
            {
                $("#headerPage").animate({left:'0px'}, 'fast');
                $("#headerPage-profile").animate({left:'80px'}, 'fast');
                $("#mainTitle").animate({opacity: '1'}, 'fast');
            }
            else
            {
                $("#headerPage").animate({left:'-250px'}, 'fast');
                $("#headerPage-profile").animate({left:'0px'}, 'fast');
                $('#mainTitle').animate({opacity: '0'}, 'fast');
            }
            isSideBarEnable = !isSideBarEnable;
        }
        //otherwise go to Profile page
        else
        {
            $('html, body').animate({scrollTop:$("#profilePage").offset().top}, 'slow');
        }
    }
});